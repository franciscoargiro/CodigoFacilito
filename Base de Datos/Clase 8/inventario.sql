-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema inventarios
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema inventarios
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `inventarios` DEFAULT CHARACTER SET utf8 ;
USE `inventarios` ;

-- -----------------------------------------------------
-- Table `inventarios`.`Clientes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventarios`.`Clientes` (
  `idClientes` INT NOT NULL,
  `NombresCliente` VARCHAR(100) NULL,
  `ApellidosCliente` VARCHAR(100) NULL,
  PRIMARY KEY (`idClientes`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `inventarios`.`Ventas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventarios`.`Ventas` (
  `idVentas` INT NOT NULL,
  `FechaVentas` DATE NULL,
  `MontoVenta` FLOAT NULL,
  `Clientes_idClientes` INT NOT NULL,
  PRIMARY KEY (`idVentas`),
  INDEX `fk_Ventas_Clientes1_idx` (`Clientes_idClientes` ASC),
  CONSTRAINT `fk_Ventas_Clientes1`
    FOREIGN KEY (`Clientes_idClientes`)
    REFERENCES `inventarios`.`Clientes` (`idClientes`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `inventarios`.`Facturas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventarios`.`Facturas` (
  `idFacturas` INT NOT NULL,
  `FechaFactura` DATE NULL,
  `MontoFactura` FLOAT NULL,
  `Ventas_idVentas` INT NOT NULL,
  `Clientes_idClientes` INT NOT NULL,
  PRIMARY KEY (`idFacturas`),
  INDEX `fk_Facturas_Ventas_idx` (`Ventas_idVentas` ASC),
  INDEX `fk_Facturas_Clientes1_idx` (`Clientes_idClientes` ASC),
  CONSTRAINT `fk_Facturas_Ventas`
    FOREIGN KEY (`Ventas_idVentas`)
    REFERENCES `inventarios`.`Ventas` (`idVentas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Facturas_Clientes1`
    FOREIGN KEY (`Clientes_idClientes`)
    REFERENCES `inventarios`.`Clientes` (`idClientes`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `inventarios`.`Productos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventarios`.`Productos` (
  `idProductos` INT NOT NULL,
  `DescripcionProducto` VARCHAR(200) NULL,
  `ClaveProducto` VARCHAR(45) NULL,
  `CodigoBarras` VARCHAR(45) NULL,
  PRIMARY KEY (`idProductos`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `inventarios`.`DetalleVentas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `inventarios`.`DetalleVentas` (
  `Productos_idProductos` INT NOT NULL,
  `Ventas_idVentas` INT NOT NULL,
  `CantidadProducto` INT NULL,
  PRIMARY KEY (`Productos_idProductos`, `Ventas_idVentas`),
  INDEX `fk_Productos_has_Ventas_Ventas1_idx` (`Ventas_idVentas` ASC),
  INDEX `fk_Productos_has_Ventas_Productos1_idx` (`Productos_idProductos` ASC),
  CONSTRAINT `fk_Productos_has_Ventas_Productos1`
    FOREIGN KEY (`Productos_idProductos`)
    REFERENCES `inventarios`.`Productos` (`idProductos`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Productos_has_Ventas_Ventas1`
    FOREIGN KEY (`Ventas_idVentas`)
    REFERENCES `inventarios`.`Ventas` (`idVentas`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
