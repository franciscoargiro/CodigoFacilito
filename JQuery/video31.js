(function($){

	$.fn.vscroll = function(){

		var objeto = $(this);
		
		var altoOriginal = $(this).height();
		var anchoOriginal = $(this).width();

		$(window).scroll(function(){
			// console.log("El Plugin se carga correctamente");
			
			var pxScroll = $(window).scrollTop();
			console.log(pxScroll);

			if (pxScroll > 400) {
				console.log('Mayor a 400');
				
				objeto.css({
					'position':'fixed',
					'left':10,
					'bottom':10,
					'height':125,
					'width':200

				});
			} else {
				console.log('Es menor a 400');

				objeto.css({
					'position': 'relative',
					//'left': 'auto',
					//'bottom': 'auto',
					'height': altoOriginal,
					'width': anchoOriginal,

				});

			}

		});	

	};

}(jQuery));