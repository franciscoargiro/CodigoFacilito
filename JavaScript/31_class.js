// El uso de class aun (29/4/17) no esta soportado por todos los navegadores y ademas debe ser utilizado en modo "use strict"

// Acá por ejemplo si no uso el modo strict funciona en chrome y firefox pero no en IE10... como siempre

"use strict"

class alumno {
	constructor (nombre){
		this.nombre = nombre;
	}
}


var alumno01 = new alumno("Francisco");

console.log(alumno01.nombre);