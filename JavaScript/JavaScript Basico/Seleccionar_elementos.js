// todas las selecciones se dan con el elemento document
alert("Prueba de que Js funcione correctamente");


function addClass(){ 
	
	var div = document.getElementById('mi_div');
	div.classList.add('mi_clase'); 
}

/*
var mi_clase = document.querySelector(".mi_clase");

console.log(mi_clase);
*/
/*
// Creo una variable llamada div y le doy el valor de referencia del ID de mi_div
var div = document.getElementById("mi_div");
//var div = document.getElementByTagName("div");


console.log(div);

// ahora le pido que a ese div le agregue la clase "mi_clase"
div.classList.add(".mi_clase");
*/

// otra forma de hacerlo es así:

// document.getElementById("mi_div").classList.add("mi_clase");

//document.getElementById("mi_div").className= "mi_clase";


