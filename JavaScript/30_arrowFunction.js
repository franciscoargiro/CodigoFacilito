// Vamos a obtrener el cuadrado de los valores de un array 

var elArray = [1,2,3,4,5];



// Forma tradicional:

var resultado = elArray.map(function(n){

	return n*n;
});

console.log(resultado);


// Con Arrow function

var resultado2 = elArray.map((n)=>{return n*n;});

console.log(resultado2);