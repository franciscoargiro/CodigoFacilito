
// https://api.github.com/users/codigofacilito


function GET(url,username) {
	return new Promise(function(resolve,reject){

		var ajaxCall = new XMLHttpRequest();

		ajaxCall.open("GET", url+username);	
		
		ajaxCall.send();

		ajaxCall.onload = function (){
			if(ajaxCall.status == 200){
				return resolve(ajaxCall.response);
			} else {
				return reject(Error(ajaxCall.status));
			}	
		};		


		// Ahora quiero obtener los repos del usuario, para lo cual tengo que parsear el JSON que esta en string
		

	});
};




function getUserInfo(url,username){
	return GET(url,username);
};


function getRepos(url,username,urlRepos){
	return GET(urlRepos);
}





var url = "https://api.github.com/users/";
var urlRepos = "repos";
var username =  "codigofacilito"; // "ciscoarg"; //

getUserInfo(url,username).then(console.log).catch(console.log);

