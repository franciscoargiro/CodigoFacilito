/*
setTimeout(mandaMensaje, 3000);


function mandaMensaje(){
	return 'Hola Promesa';
}


var mensaje = mandaMensaje();

console.log(mensaje);
*/

var promise = new Promise(function(resolve, reject){
						setTimeout(function(){
							//return "Hola Mundo";
						if(confirm("Desea cumplir la promes?")){	
							return resolve("Hola Mundo");
						} else {	
							reject(new Error("Hubo un error"));
						}
					},2000);
				});


promise.then(function(resultado){
	console.log(resultado);
},function(error){
	console.log("Algo salió mal");
	console.log(error);
});