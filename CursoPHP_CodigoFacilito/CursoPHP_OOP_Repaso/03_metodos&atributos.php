<?php 

	/**
	* 
	*/
	class Persona
	{
		// Atributos
		public $nombre = array();
		public $aprellido = array();

		// Metodos
		public function guardar($name, $surname){
			// Guarda los nombres y apellidos 	
			$this->nombre[] = $name;
			$this->apellido[] = $surname; 
		}


		public function mostrar(){
			// Muestra los nombres y apellidos guardados
			for ($i=0; $i < count($this->nombre) ; $i++) { 
				//echo "Nombre: " . $this->nombre[$i] . " / Apellido: " . $this->apellido[$i] . "<br>";
				$this->formato($this->nombre[$i], $this->apellido[$i]);
			}
		}
	

		private function formato($nombre, $apellido){
			// Le damos el formato a la funcion mostrar
			echo "Nombre: " . $nombre . " / Apellido: " . $apellido . "<br>";
		}

	}


	$persona = new Persona();

	$persona->guardar("Francisco","Argiro");
	$persona->guardar("Agustina","Mongiello");

	$persona->mostrar();

	//El siguiente metodo no se va a poder mostrar por ser privado y por lo tanto interno de la clase:
	$persona->formato("Ignacio", "Mugica");



 ?>