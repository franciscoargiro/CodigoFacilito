<?php 

	/**
	* Estamos probando las clases con atributos estaticos
	*/
	class Pagina 
	{
		// aTRIBUTOS
		public $nombre = "Curso 07 de PHP";
		public static $url = "www.codigofacilito.com";


		// Metodos
		public function bienvenidos(){
			echo "Bienvienidos a " . $this->nombre . " en el sitio " . Pagina::$url;
		}

		public static function despedida(){
			//echo "Adios desde el " . $this->nombre . " en el sitio " . Pagina::$url;
			echo "<br> Adios desde el sitio " . Pagina::$url;
		} 

	}



	$pagina = new Pagina();

	$pagina->bienvenidos();


	//echo "</br>".$pagina->nombre;
	//echo "</br>".$pagina->url;

	$pagina->despedida();



 ?>