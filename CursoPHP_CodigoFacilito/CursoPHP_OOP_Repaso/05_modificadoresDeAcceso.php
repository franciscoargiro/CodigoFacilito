<?php 

	/**
	*  Vamos a ver con esta funcion como funciona protected
	*/
	class Facebook
	{
		// Atributors
		public $nombre;
		public $edad;
		private $contrasena;


		// Metodos
		
		function __construct($name, $edge, $pass)
		{
			$this->nombre = $name;
			$this->edad = $edge;
			$this->contrasena = $pass;
		}	


		public function verInfo()
		{
			echo "Nombre: ". $this->nombre . "<br>" .
				 "Edad: ". $this->edad . "<br>" .
				 "Password: ". $this->contrasena . "<br>";				 
		}

	}




	$ctaFacebook = new Facebook("Francisco", 32, "1234");
	
	// puedo acceder al atributo private porque es a traves de un metodo de una funcion
	$ctaFacebook->verInfo();

	// si lo llamo desde afuera no me da acceso y me genera un error
	echo "-------------------- <br>
		 Nombre: ". $ctaFacebook->nombre . "<br>" .
		 "Edad: ". $ctaFacebook->edad . "<br>";
		 //"Password: ". $ctaFacebook->contrasena . "<br>";		// lo tuve que comentar o me da error		 
		


 ?>





