<?php

declare(strict_types=1);

#require("persona.php");
#include("persona.php");

/*#comento estos archivos de require_once porque utilizo el autocarga
require_once 'clases/persona.php';
require_once 'clases/cliente.php';
require_once 'clases/proveedor.php';
*/

#utilizamos el archivo autoload.php para hacer una autocarga de las clases que se requieren
require_once "autoload.php";
$load= new AutoLoader();
$load->load();


#llamamos donde la carpeta donde estan los namespace y luego los escapamos
use clases as clas;

$pedro= new clas\Cliente("Pedro");

$juan= new clas\Cliente("Juan");

echo "<br>";

$pedro->accion("presentarse");

echo "<br>";

echo clas\Persona::$color;

echo "<br>";

/*
$pedro->apodo = "Cacho";
echo "Mi apodo es $pedro->apodo";
*/

$pedro->setApodo("22");
echo "Mi apodo es ".$pedro->getApodo();

echo "<br>";
echo "<br>PROVEEDOR: <br>";
$pancho= new clas\Proveedor("Matias","Ale");
echo "<br>";
$pancho->decirApellido();


$pancho->setMercaderia("Alfombras");
$pancho->mostrarMerca();




 ?>
