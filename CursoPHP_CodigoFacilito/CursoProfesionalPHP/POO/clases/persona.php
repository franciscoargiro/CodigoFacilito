<?php

namespace clases;


class Persona{
  private $name;
  private $lastname;
  private $sex;
  private $nacionalidad;
  private $age;
  public static $color="rojo";


  /*
  function __construct($nombre, $apellido, $sexo, $pais, $edad)
  {
    $this->name= $nombre;
    $this->lastname = $apellido;
    $this->sex=$sexo;
    $this->nacionalidad = $pais;
    $this->age=$edad;
  }
  */


# Función de acceso
  function accion($orden){
    if (empty($orden)) {
      echo "Debes indicarme que hacer";
    } else {
      #echo "variable llena";
      $this->procesarOrden($orden);
    }
  }


  private function procesarOrden($orden){
    switch ($orden) {
      case 'correr':
      echo "$this->name está corriendo";
      break;

      case 'presentarse':
      $this->presentarse();
      break;

      case 'edad':
      echo "Mi edad es ";$this->age;
      break;

      default:
      echo "No comprendo que quieres que haga";
      break;
    }
  }


#Funciones varias
  private function presentarse(){
    echo "Hola, mi nombre completo es $this->name y tengo $this->age años.";
  }


  function algoSobreTi(){
    echo "Mi color preferido es ".self::$color;
  }










    /**
     * Get the value of Name
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param mixed name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get the value of Lastname
     *
     * @return mixed
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set the value of Lastname
     *
     * @param mixed lastname
     *
     * @return self
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get the value of Sex
     *
     * @return mixed
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set the value of Sex
     *
     * @param mixed sex
     *
     * @return self
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get the value of Nacionalidad
     *
     * @return mixed
     */
    public function getNacionalidad()
    {
        return $this->nacionalidad;
    }

    /**
     * Set the value of Nacionalidad
     *
     * @param mixed nacionalidad
     *
     * @return self
     */
    public function setNacionalidad($nacionalidad)
    {
        $this->nacionalidad = $nacionalidad;

        return $this;
    }

    /**
     * Get the value of Age
     *
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * Set the value of Age
     *
     * @param mixed age
     *
     * @return self
     */
    public function setAge($age)
    {
        $this->age = $age;

        return $this;
    }

    /**
     * Get the value of Color
     *
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set the value of Color
     *
     * @param mixed color
     *
     * @return self
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

}












 ?>
