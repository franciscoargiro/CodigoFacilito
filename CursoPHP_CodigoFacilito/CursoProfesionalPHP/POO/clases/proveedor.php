<?php

declare(strict_types=1);

namespace clases;

require_once 'persona.php';



  /**
   * la clase Proveedor es el que nos va a vender mercaderíarray
   */
  class Proveedor extends Persona
  {

      private $mercaderia;

    function __construct(String $nombre, String $apellido)
    {
        $this->setName($nombre);
        $this->setLastname($apellido);
        echo "Mi nombre es {$this->getName()}";

    }


    public function decirApellido(){
      echo "Mi apellido es {$this->getLastname()}";
    }


    public function mostrarMerca(){

      echo "Lo que vendo son $this->mercaderia";

    }



    /**
     * Get the value of la clase Proveedor es el que nos va a vender mercaderíarray
     *
     * @return mixed
     */
    public function getMercaderia()
    {
        return $this->mercaderia;
    }

    /**
     * Set the value of la clase Proveedor es el que nos va a vender mercaderíarray
     *
     * @param mixed mercaderia
     *
     * @return self
     */
    public function setMercaderia($mercaderia)
    {
        $this->mercaderia = $mercaderia;

        return $this;
    }

}




 ?>
