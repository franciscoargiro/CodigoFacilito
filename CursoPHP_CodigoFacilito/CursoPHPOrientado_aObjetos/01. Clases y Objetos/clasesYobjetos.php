<?php

	class Persona{
		//Atributos
		public $nombre = "Pedro";
		//Metodos
		public function hablar($mensaje){
			echo "Saludos ".$mensaje;
		}
	}


	$persona = new Persona(); // creamos el objeto

	echo $persona->nombre; // llamamos un atributo

	$persona->hablar(" desde el escritorio"); // utilizamos un metodo del objeto

	