<?php

	
	// CREO LA CLASE TANQUE
	 
	class Tanque
	{
		// Atributos
		//public $sensor="0001534456879";
		public $sensor;
		public $nivel;
		public $temperatura;
		public $litrosdeldia;
		public $día;

		// Métodos

		public function ultimas24hs () {
			echo "Arroja los datos de las ultimas 48 hs:<br>";
			echo "Nivel: ".$this->nivel."cm.<br>";
			echo "Temperatura ".$this->temperatura."C°<br>";
			echo "Litros: ".$this->litrosdeldia."<br>";
		}

		public function buscarFecha($dia){
			echo "Arroja los datos del día ".$dia.":<br>";
			echo "Nivel: ".$this->nivel."cm.<br>";
			echo "Temperatura ".$this->temperatura."C°<br>";
			echo "Litros: ".$this->litrosdeldia."<br>";
		}	

	}


	//  CREO UN OBJETO LLAMADO TANQUE01

	$tanque01 = new Tanque();

	// Le asignamos valores a sus atributos:

	$tanque01->sensor="0001534456879";
	$tanque01->nivel="140";
	$tanque01->temperatura="4";
	$tanque01->litrosdeldia="4300";

	// Llamamos a sus métodos:

	$tanque01->ultimas24hs();

	echo "-----<br><br>";

	$fecha="20/03/16";
	$tanque01->buscarFecha($fecha);

	// Al probar el codigo, no me imprime los valores de los atributos, voy a continuar con el curso a ver cual es mi error.