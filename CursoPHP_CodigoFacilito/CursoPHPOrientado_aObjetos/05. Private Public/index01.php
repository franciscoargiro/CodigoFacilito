<?php

	class Facebook {

		// Atributos

		public $nombre;
		public $edad;
		private $pass; // contraseña

		// Metodos

		public function __construct($nombre,$edad,$pass) {
			$this->nombre = $nombre;
			$this->edad = $edad;
			$this->pass = $pass;
		}

		public function mostrar() {
			echo "Nombre: ".$this->nombre."<br>";
			echo "Edad: ".$this->edad."<br>";
			echo "Password: ".$this->pass;
		}

		public function renovarPass($renPass) {
			$this->cambiarPass($renPass);
		}

		private function cambiarPass($newPass) {
			$this->pass= $newPass;
		}

	}


	$facebook = new Facebook("Fran",30,1234);

	$facebook->mostrar();


	echo "<br><br>".$facebook->nombre."<br><br>";
	//echo "<br><br>".$facebook->pass;

	/*	
	$facebook->cambiarPass(4321);
	$facebook->mostrar();
	*/

	$facebook->renovarPass(4567);
	$facebook->mostrar();



?>