<?php


// CREO LA INTERFACE QUE OBLIGA A LA CLASE A UTILIZAR ESTOS METODOS

interface Vehiculo {
	public function estado(); // Motor encendido? Litros en tanque?
	public function llenarTanque($lts);
	public function encender();
	public function apagar();
}

interface Moto extends Vehiculo {
	public function usar($km);
	
}


// CREO UNA CLASE TIPO Moto

Class CBR600 implements Moto {

	// Atributos
		private $motor=false;
		private $tanque=0;

	// Metodos
		public function estado(){
			if($this->motor) {
				echo "El motor esta encendido y tiene ".$this->tanque." litros.";
			} else {
				echo "Motor esta apagado y tiene ".$this->tanque." litros.";	
			}
		}
		public function llenarTanque($lts){
			if (($this->tanque+$lts) > 50){
					echo "El tanque puede cargar max. 50 Lts., se han cargado ". (50-$this->tanque) ." Litros y esta lleno";
					$this->tanque=50;
			} else {		
					$this->tanque= $this->tanque+$lts;
					echo "Se han cargado ".$lts." Litros.";	
			}
		}
		public function encender(){
			if($this->motor) {
				echo "El motor ya esta encendido";
			} else {
				if($this->tanque<=0) {
					echo "El tanque esta vacio, necesita cargar combustible para encenderlo.";
				} else {
				$this->motor=true;
				echo "Motor encendido";
				}
			}	
		}
		public function apagar(){
			if ($this->motor) {
				$this->motor=false;
				echo "Motor apagado";
			} else {
				echo "El motor ya se encuentra apagado";
			}
		}
		public function usar($km){
			
			// 100km cad 8 lts, asi que 1 lts = 12,5Km 
			
			if($this->motor) {
				if (($this->tanque-($km/12.5))<0) {
				echo "Se a quedado sin gasolina";
				$this->tanque=0;
				$this->motor=false;
				} else {
				$this->tanque= $this->tanque-($km/12.5);
				echo "Se han consumido ".$km/12.5 . " Litros.";} 
			} else {
				echo "No se puede usar porque el motor esta apagado.";
			}
			
		}

	}


	// CREO UN NUEVO OBJETO PARA PROBAR LOS METODOS

	$CBR600 = new CBR600;

	$CBR600->estado();
	echo "<br>";
	$CBR600->encender();
	echo "<br>";
	$CBR600->llenarTanque(40);
	echo "<br>";
	$CBR600->llenarTanque(20);
	echo "<br>";
	$CBR600->encender();
	echo "<br>";
	$CBR600->usar(40);
	echo "<br>";
	$CBR600->estado();
	echo "<br>";
	$CBR600->apagar();
	echo "<br>";
	$CBR600->usar(40);
	echo "<br>";
	$CBR600->encender();
	echo "<br>";
	$CBR600->usar(400);
	echo "<br>";
	$CBR600->estado();
	echo "<br>";
	$CBR600->usar(200);
	echo "<br>";
	$CBR600->estado();
	echo "<br>";
	
	
?>	