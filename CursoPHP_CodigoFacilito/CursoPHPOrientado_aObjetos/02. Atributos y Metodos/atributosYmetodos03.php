<?php

	class Persona {

		// atributos

		public $nombre= array();
		public $apellido= array();

		// Metodos

		public function guardar($nombre, $apellido) {

			$this->nombre[]= $nombre;
			$this->apellido[]= $apellido;
		}

		public function mostrar() {

			for ($i=0; $i<(count($this->nombre)); $i++) {
				echo "Nombre: ".$this->nombre[$i]." | Apellido: ".$this->apellido[$i]."<br><br>";
			}
		}

	}



	$persona= new Persona;

	$persona->guardar("Fran","Argiro");
	$persona->guardar("Sari", "Argiro");
	$persona->guardar("Martin","Argiro");
	$persona->guardar("Mechi","Argiro");

	$persona->mostrar();


?>


