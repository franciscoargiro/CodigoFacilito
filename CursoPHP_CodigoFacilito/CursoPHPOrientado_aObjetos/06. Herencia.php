<?php

	
	Class Vehiculo {

		// Atributos
		 public $motor= false;
		 public $color;
		 public $marca;

		 // Metodos

		 public function estado(){
		 	if($this->motor==false){
		 		echo "El motor esta apagado";
		 	} else {
		 		echo "El motor esta encendido";
		 	}
		 }

		 public function encender() {
		 	if ($this->motor==true){
		 		echo "El motor ya esta encendido";
		 	} else {
		 		$this->motor=true;
		 		echo "Motor encendido";
		 	}
		 }

		 public function apagar() {
		 	if ($this->motor==true){
		 		$this->motor=false;
		 		echo "Motor apagado";
		 	} else {
		 		echo "El motor ya esta apagado";
		 	}
		 }
	}

	echo "PRUEBA CLASE Vehiculo: <br><br>";
	$vehiculo = new Vehiculo;
	$vehiculo->estado();
	echo "<br>";
	$vehiculo->encender();
	echo "<br>";
	$vehiculo->estado();
	echo "<br>";
	$vehiculo->apagar();
	echo "<br>";
	$vehiculo->estado();
	echo "<br>";
	$vehiculo->apagar();
	echo "<br>";
	$vehiculo->encender();
	echo "<br>";
	$vehiculo->encender();
	echo "<br>";
	echo "<br>";
		echo "--------------";
	echo "<br>";
	echo "<br>";		


	// CREANDO LA SUB-CLASE MOTO

	Class Moto extends Vehiculo {

		// Atributos
			public $cilindrada;
			public $acientos;

		// Metodos

			public function willy(){
				if($this->motor==true){
					echo "Haciendo willy!!";
				} else {
					echo "Es necesario encender el motor primero";
				}
			}	

	} 



echo "PRUEBA SUB-CLASE Moto: <br><br>";
$moto = new Moto;

$moto->estado();
echo "<br>";
$moto->willy();
echo "<br>";
$moto->encender();
echo "<br>";
$moto->willy();
echo "<br>";


?>






