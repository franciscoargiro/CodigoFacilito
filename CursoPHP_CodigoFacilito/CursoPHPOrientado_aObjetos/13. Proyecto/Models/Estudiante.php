<?php namespace Models;


	class Estudiante{
		private $id;
		private $nombre;
		private $edad;
		private $promedio;
		private $imagen;
		private $id_seccion;
		private $fecha;

		private $con;

		
		public function __construct(){
			$this->$con = new Conexion();  // La clase Conexion la vamos a traer con el Autoload 
		}
	
		
		// Funcion de prueba
		public function hablar($mensaje){
			echo $mensaje;
		}


		public function set($atributo,$contenido) {
			$this->$atributo=$contenido;
		}
		
		public function get($atributo){
			return $this->$atributo;
		}
		

		public function listar(){
		 	$sql="SELECT t1.*, t2.nombre as nombre_seccion FROM estudiantes t1 INNER JOIN secciones t2 ON t1.id_seccion= t2.id";  // APRENDER ESTAS SENTENCIA EN CURSO PHPMYADMIN
		 	/*
		 	ESTO SE LEE ASÍ: Seleccionar todo de tabla 1 y nombre de tabla 2 como nombre_seccion donde estudiantes tabla 1 relacionado con secciones tabla 2 en donde id_seccion de tabla 1 es igual al id de la tabla 2
		 	*/
		 	$datos= $this->con->consultaRetorno($sql);
		 	return $datos; // esto lo dejo comentado porque en la funcion consultaRetorno() ya existe el return. 
		 }

		 public function add(){
		 	$sql= "INSERT INTO estudiantes(id, nombre, edad, promedio, imagen, id_seccion, fecha) VALUES (null, '{$this->nombre}', '{$this->edad}','{$this->promedio}', '{$this->imagen}', '{$this->id_seccion}', NOW()"; 

		 	$this->con->consultaSimple($sql);
		 	return $datos; // Lo mismo que lo anterior, creo que acá se repite codigo
		 }

		 public function delate(){
		 	$sql= "DELETE FROM estudiantes WHERE id = '{$this->id}'";
		 	$this->con->consultaSimple($sql);		 	
		 }

		 public function edit(){
		 	$sql="UPDATE FROM estudiantes SET nombre = '{$this->nombre}', edad='{$this->edad}', promedio='{$this->promedio}', id_seccion='{$this->id_seccion}'";
		 	$this->con->consultaSimple($sql);
		 }

		 public function view(){
		 	$sql= "SELECT t1.*, t2.nombre as nombre_seccion FROM estudiantes t1 INNER JOIN secciones t2 ON t1.id_seccion = t2.id WHERE t1.id='{$this->id}'";
		 	//$datos= $this->con->consultaRetorno($sql);
		 	$this->con->consultaRetorno($sql); // yo lo hago así porque la funcion consultaRetorno($sql) de la clase Conexion ya genera el retorno $datos.
		 	$row= mysql_fetch_assoc($datos);
		 	return $row; // Lo mismo que lo anterior, creo que acá se repite codigo
		 }

	}
