<?php 

namespace Models;

	Class Conexion{
			
		private $datos = array(
			'host' => "localhost",
			'user' => "root",
			'pass' => "fragiro789987",
			'db' => "proyectocodfac"
		);

		private $con;

		public function __construct(){
			$this->con = new \mysqli($this->datos['host'],$this->datos['user'],$this->datos['pass'],$this->datos['db']);
		}

		public function consultaSimple($sql){
			$this->con->query($sql);
		}

		public function consultaRetorno($sql){
			$datos= $this->con->query($sql);
			return $datos; // para mi acá le erro porque $datos ya existe aunque el otro es un array
		}

	}

?>	