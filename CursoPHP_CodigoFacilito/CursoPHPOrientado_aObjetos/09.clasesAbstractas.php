<?php

	
	abstract Class Molde {
		abstract function nombrar($nombre);
		abstract function obtenerNombre();

	}


	Class Persona extends Molde {

		public $nombre;

		public function nombrar($nombre){
			$this->nombre=$nombre;
		}

		public function obtenerNombre(){
			print($this->nombre);
		}
	}


$obj= new Persona;
$obj->nombrar("fran");
$obj->obtenerNombre();	

