<?php
	
	/*
	 
	 // Esto no se puede hacer porque es para llamar a Clases no a traits, lo que si se puede hacer es un archivo de traits eh incluirlos una vez 
		
	 function autoload($trait) {
	 	include "traits/".$trait."php";
	 }


	 spl_autoload_register('autoload');
	*/


	// TRAITS	

	 trait caract {
	 	public $color;
	 	public $altura;
	 	public $edad;
	 }



	 trait habilidades{
	 	public function hablar($mensaje){
	 		echo $mensaje;
	 	}

	 	public function saltar(){
	 		echo "SALTO!!";
	 	}
	 }

	
	
	 // CREAMOS UNA CLASE Y USAMOS LOS TRAITS


	Class Persona {
		use habilidades, caract;

	} 
	 


	// INSTANCIAMOS LA CLASE Y LA PROBAMOS 
	
	$obj = new Persona;
	$obj->saltar();
	echo "<br>";
	Persona::hablar("Hola viaja como estas!");	
	echo "<br>";
	$obj->color="amarrillo";
	echo $obj->color;

