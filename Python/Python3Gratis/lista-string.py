my_string="Curso Facilito!"


print(my_string)
print(my_string[0])
print(my_string[-1]) #comienzo desde el ultimo caracter
print(my_string[0:10])
print(my_string[0:10:2]) #pego saltos de dos en dos
print(my_string[::-1])  # invierte la lectura