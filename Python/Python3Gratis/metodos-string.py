coures="curso"
my_string="codigo Facilito!"

result ='{} de {}'.format(coures, my_string)

print(result)


result ='{a} de {b}'.format(b=coures, a=my_string)

print(result)

print("------------------")


result= result.lower() #paso el string a minusculas
print(result)


result= result.upper() #paso el string a mayusculas
print(result)


result= result.title() #lo pasa a un formato de titulo con cada primer caractar en mayusucula
print(result)


print("------------------")


"""Esta es otra forma de comentar
pero conmultilinea """

#BUSQUEDA:

pos = result.find("De")  #pongo De con mayuscula porque fue el ultimo resultado que generamso
print(pos)  # el resultado es 17 que es la posición del caracter dentro del string


count = result.count("c")
print(count)  # se repite una sola vez porque estan en mayusculas las otras

""" si lo paso todo a minusculas el resultado va a ser otro """

result = result.lower()
count = result.count("c")
print(count) # en este caso el resultado es 3 en vez de 1 porque pase todas las C a minusculas

print("------------------")


new_string = result.replace("c","x") #reemplazo c por x en todo el string
print(new_string)

new_string = result.split(" ") #secciono las partes que estan divididas por un ESPACIO
print(new_string)












