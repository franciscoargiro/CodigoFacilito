fruta = "manzana"

"""
if fruta == "kiw" : 
	print("el valor es igual")
else: 
	print("Es distinto")	


mensaje = "El valor es kiwi" if fruta == "kiwi" else "El valor es distinto"
print(mensaje)
"""
"""
if fruta   == "kiwi" : 
	print("Es un kiwi")
elif fruta == "manzana":
	print("Es una manzana")
else: 
	print("Es otra fruta")	
"""


#  SI NO QUIERO QUE SE EJECUTE EL CODIGO PONGO PASS:

if fruta   == "kiwi" : 
	print("Es un kiwi")
elif fruta == "manzana":
	pass	# print("Es una manzana")
else: 
	print("Es otra fruta")	


