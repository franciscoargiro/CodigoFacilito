''' al igual que las listas y las tuplas, los diccionarios nos permiten almacenar datos,
 la diferencia es que no utiliza indices, lo que necesitan es una clave, parecido a JSON 

	los datos que se utilizan en los diccionarios deben ser inmutables
	
	a variables repetidas toma la ultima
	
	los diccionarios pueden crecer o decrecer



'''

diccionario = {'a':True,'e': False}
print(diccionario['a'])				

diccionario['b']="Pato"
print(diccionario)

diccionario['a']=False
print(diccionario['a'])

valor = diccionario['b']
print(valor)

# print(diccionario['z']) # Esto produce un error porque Z no existe, entonces hacemos lo siguiente
print(diccionario.get('z',"no se encontro"))

# para borrar un elemento utilizamos DEL
print(diccionario)
del diccionario['b']
print(diccionario)


# llaves: objetos iterables  (nos regresa los nombres de las claves, o llaves, peor no su valor)
llaves = diccionario.keys()
print(llaves)


lista_pura = list(llaves)
print(lista_pura)

llaves_tuples = tuple(lista_pura)
print(llaves_tuples)

valores_tuples = tuple(diccionario.values())
print(valores_tuples)



diccionario_2 = {'z':23, 'w':88}

diccionario.update(diccionario_2)
print(diccionario)

'''
tb podria haber hecho una por una:

	diccionario['z'] = diccionario_2['z']   --> pero esto es muy incomodo y es preferible el update

'''










